const express = require('express');
const fileUpload = require('express-fileupload');
const bodyParser = require('body-parser');
const app = express();
const path = require('path');
const router = express.Router();
const fs = require("fs");
const JSZip = require("jszip");
const port = process.env.port || 20106
const saveAs = require('file-saver')
const amqp = require('amqplib/callback_api');

app.use(bodyParser.json());         // to support JSON-encoded bodies
app.use(bodyParser.urlencoded({     // to support URL-encoded bodies
    extended: true
})); 

app.use(fileUpload({
    limits: { fileSize: 50 * 1024 * 1024 },
}));

router.post('/', (req, res) => {
    const routingKey = req.header('X-ROUTING-KEY');
    const file = req.files.file;
    const fileName = file.name;
    const data = file.data;
    const realData = JSON.parse(data.toString()).data;
    if(!req.files) {
        res.send({
            status: false,
            message: 'No file uploaded'
        });
    } else {
        //send response
        res.set('X-ROUTING-KEY', routingKey)
        res.send({
            status: true,
            message: 'File is uploaded',
            data: {
                name: file.name,
                mimetype: file.mimetype,
                size: file.size
            }
        });
        // zip file
        zipFile(fileName, Buffer.from(realData.data), routingKey)
        
    }
});

async function zipFile(fileName, data, routingKey) {
    amqp.connect('amqp://0806444524:0806444524@152.118.148.95:5672/%2f0806444524', function(error0, connection) {
        if (error0) {
            throw error0;
        }
        connection.createChannel(function(error1, channel) {
            if (error1) {
                throw error1;
            }
            var exchange = '1606879230';
            var msg = "Starting zipping file..."
            channel.assertExchange(exchange, 'direct', {
                durable: false
            });
            channel.publish(exchange, routingKey, Buffer.from(msg))
    
            const zip = new JSZip();
            zip.file(fileName, data);
            // ... and other manipulations
            // zip
            zip.generateNodeStream({type:'nodebuffer', streamFiles:true})
            .pipe(fs.createWriteStream('out.zip'))
            .on('finish', function () {
                // JSZip generates a readable stream with a "end" event,
                // but is piped here in a writable stream which emits a "finish" event.
                console.log("out.zip written.");
                var msg = "Zipping file completed"
                channel.publish(exchange, routingKey, Buffer.from(msg))
            });
            setTimeout(function() {
                connection.close();
            },  500);
        });
    });
}

app.use('/', router);
app.listen(port);

console.log(`Example app listening on port ${port}!`);
